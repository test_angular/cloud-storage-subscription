import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuccessfulSubmitComponent } from './components/successful-submit/successful-submit.component';
import { SubscriptionConfirmationRootComponent } from './containers/subscription-confirmation-root/subscription-confirmation-root.component';
import { SubscriptionOptionsRootComponent } from './containers/subscription-options-root/subscription-options-root.component';
import { SubscriptionPaymentRootComponent } from './containers/subscription-payment-root/subscription-payment-root.component';
import { SubscriptionRootComponent } from './containers/subscription-root/subscription-root.component';
import { PaymentInfosReceivedGuard } from './guards/payment-infos-received.guard';

const routes: Routes = [
    {
        path: '',
        component: SubscriptionRootComponent,
        children: [
            {
                path: '',
                redirectTo: 'options',
                pathMatch: 'full',
            },
            {
                path: 'options',
                component: SubscriptionOptionsRootComponent,
            },
            {
                path: 'payment',
                component: SubscriptionPaymentRootComponent,
            },
            {
                path: 'confirmation',
                component: SubscriptionConfirmationRootComponent,
                canActivate: [PaymentInfosReceivedGuard],
            },
        ],
    },
    {
        path: 'successful',
        component: SuccessfulSubmitComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SubscriptionRoutingModule {}
