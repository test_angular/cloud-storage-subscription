import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { SubSink } from 'subsink';
import { STEPS } from '../../constants/steper.constant';
import { Step } from '../../types/step.interface';

@Component({
    selector: 'app-stepper',
    templateUrl: './stepper.component.html',
    styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent implements OnDestroy, OnChanges {
    currentStep: Step;

    @Input() steps: Step[];

    private subs = new SubSink();

    constructor(private router: Router) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['steps']?.currentValue && this.steps?.length) {
            this.currentStep = this.steps[0];
        }
    }

    ngOnInit() {
        this.subs.sink = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                const step = STEPS.find((s) => s.path === event.url);
                step && (this.currentStep = step);
            }
        });
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }
}
