import { Component, Input } from '@angular/core';
import { CloudSubscription } from '../../types/cloud-subscription.interface';

@Component({
    selector: 'app-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent {
    @Input() subscription: CloudSubscription;
}
