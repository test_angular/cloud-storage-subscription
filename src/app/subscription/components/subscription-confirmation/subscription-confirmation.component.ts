import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CloudSubscription } from '../../types/cloud-subscription.interface';

@Component({
    selector: 'app-subscription-confirmation',
    templateUrl: './subscription-confirmation.component.html',
    styleUrls: ['./subscription-confirmation.component.scss'],
})
export class SubscriptionConfirmationComponent {
    form: FormGroup;

    constructor(private formBuilder: FormBuilder) {}

    @Input() set subscription(subscription: CloudSubscription) {
        if (subscription && !this.form) {
            this.form = this.initForm(subscription);
        }
    }

    @Output() confirm: EventEmitter<string> = new EventEmitter<string>();

    onSubmit(form: FormGroup) {
        this.confirm.emit(form.value.clientEmail);
    }

    private initForm(subscription: CloudSubscription): FormGroup {
        return this.formBuilder.group({
            clientEmail: [subscription?.clientEmail, [Validators.required, Validators.email]],
            accept: [false],
        });
    }
}
