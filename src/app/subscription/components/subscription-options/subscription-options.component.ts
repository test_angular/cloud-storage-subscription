import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import { SUBSCRIPTION_DURATION_OPTIONS, SUBSCRIPTION_GIGABYTE_OPTIONS } from '../../constants/subscription.constants';
import { CloudSubscription } from '../../types/cloud-subscription.interface';

@Component({
    selector: 'app-subscription-options',
    templateUrl: './subscription-options.component.html',
    styleUrls: ['./subscription-options.component.scss'],
})
export class SubscriptionOptionsComponent implements OnDestroy {
    form: FormGroup;
    durationOptions = SUBSCRIPTION_DURATION_OPTIONS;
    gigabyteOptions = SUBSCRIPTION_GIGABYTE_OPTIONS;

    private subs = new SubSink();

    constructor(private formBuilder: FormBuilder) {}

    @Input() set subscription(subscription: CloudSubscription) {
        this.form = this.initForm(subscription);
        this.subs.sink = this.form.valueChanges.subscribe((value) => this.formChange.emit(value));
    }

    @Output() formChange: EventEmitter<CloudSubscription> = new EventEmitter<CloudSubscription>();

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }

    private initForm(subscription: CloudSubscription): FormGroup {
        return this.formBuilder.group({
            duration: [subscription.duration, Validators.required],
            gigabyte: [subscription.gigabyte, Validators.required],
            upfrontPayment: [subscription.upfrontPayment, Validators.required],
        });
    }
}
