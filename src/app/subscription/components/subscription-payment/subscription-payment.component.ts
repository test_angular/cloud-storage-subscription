import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CloudSubscription, Payment } from '../../types/cloud-subscription.interface';

@Component({
    selector: 'app-subscription-payment',
    templateUrl: './subscription-payment.component.html',
    styleUrls: ['./subscription-payment.component.scss'],
})
export class SubscriptionPaymentComponent {
    form: FormGroup;

    constructor(private formBuilder: FormBuilder) {}

    @Input() set subscription(subscription: CloudSubscription) {
        if (subscription && !this.form) {
            this.form = this.initForm(subscription.payment);
        }
    }

    @Output() submit: EventEmitter<Payment> = new EventEmitter<Payment>();

    onSubmit(form: FormGroup) {
        this.submit.emit(form.value);
    }

    private initForm(payment: Payment): FormGroup {
        return this.formBuilder.group({
            creditCardNo: [payment?.creditCardNo, [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
            creditCardExpiration: [
                payment?.creditCardExpiration,
                [Validators.required, Validators.pattern(/^(0[1-9]|1[0-2])\/?([0-9]{2})$/)],
            ],
            creditCardSecurityCode: [
                payment?.creditCardSecurityCode,
                [Validators.required, Validators.minLength(3), Validators.maxLength(3)],
            ],
        });
    }
}
