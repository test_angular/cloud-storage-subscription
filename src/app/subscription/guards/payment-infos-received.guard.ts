import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, map } from 'rxjs';
import { SubscriptionState } from '../store/reducers/subscription.reducers';
import { getPayment } from '../store/selectors/subscription.selectors';

@Injectable()
export class PaymentInfosReceivedGuard {
    constructor(private store: Store<SubscriptionState>) {}
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.store.pipe(
            select(getPayment),
            map((payment) => !!payment)
        );
    }
}
