import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap } from 'rxjs';
import { SubscriptionMockService } from '../../services/subscription-mock.service';
import { CloudSubscription } from '../../types/cloud-subscription.interface';
import { createSubscription, createSubscriptionFail, createSubscriptionSuccess } from '../actions/subscription.action';

@Injectable()
export class SubscriptionEffects {
    constructor(private action$: Actions, private subscriptionService: SubscriptionMockService, private router: Router) {}

    createSubscription$ = createEffect(() =>
        this.action$.pipe(
            ofType(createSubscription),
            switchMap((props: { payload: CloudSubscription }) =>
                this.subscriptionService.createSubscription(props.payload).pipe(
                    map((subscription) => {
                        this.router.navigate(['/subscription/successful']);
                        return createSubscriptionSuccess({ payload: subscription });
                    }),
                    catchError(() => of(createSubscriptionFail()))
                )
            )
        )
    );
}
