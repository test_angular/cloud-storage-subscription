import { createFeatureSelector, createSelector } from '@ngrx/store';
import { SubscriptionState } from '../reducers/subscription.reducers';

const getSubscriptionState = createFeatureSelector<SubscriptionState>('subscription');

export const getSubscription = createSelector(getSubscriptionState, (state: SubscriptionState) => state.subscription);

export const getPayment = createSelector(getSubscriptionState, (state: SubscriptionState) => state?.subscription?.payment);
