import { createAction, props } from '@ngrx/store';
import { CloudSubscription } from '../../types/cloud-subscription.interface';

export const saveSubscriptionToStore = createAction(
    '[Subscription] Save Subscription To Store',
    props<{ payload: CloudSubscription }>()
);

export const createSubscription = createAction('[Subscription] Create Subscription', props<{ payload: CloudSubscription }>());

export const createSubscriptionSuccess = createAction(
    '[Subscription] Create Subscription Success',
    props<{ payload: CloudSubscription }>()
);

export const createSubscriptionFail = createAction('[Subscription] Create Subscription Fail');
