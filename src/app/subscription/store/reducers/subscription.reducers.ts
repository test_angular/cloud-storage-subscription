import { Action, createReducer, on } from '@ngrx/store';
import { DEFAULT_SUBSCRIPTION } from '../../constants/subscription.constants';
import { CloudSubscription } from '../../types/cloud-subscription.interface';
import { computeSubscriptionFinalAmount, getApplicableSubscriptionPlan } from '../../utils/subscription-amount-calculation.utils';
import { createSubscriptionSuccess, saveSubscriptionToStore } from '../actions/subscription.action';

export interface SubscriptionState {
    subscription: CloudSubscription;
}

const initialState: SubscriptionState = {
    subscription: {
        ...DEFAULT_SUBSCRIPTION,
        plan: getApplicableSubscriptionPlan(DEFAULT_SUBSCRIPTION),
        finalAmount: computeSubscriptionFinalAmount(DEFAULT_SUBSCRIPTION),
    },
};

const saveSubscriptionToStoreReducer = (state: SubscriptionState, props: { payload: CloudSubscription }) => ({
    ...state,
    subscription: {
        ...props.payload,
        plan: getApplicableSubscriptionPlan(props.payload),
        finalAmount: computeSubscriptionFinalAmount(props.payload),
    },
});

const createSubscriptionSuccessReducer = (state: SubscriptionState, props: { payload: CloudSubscription }) => initialState;

const reducer = createReducer(
    initialState,
    on(saveSubscriptionToStore, saveSubscriptionToStoreReducer),
    on(createSubscriptionSuccess, createSubscriptionSuccessReducer)
);

export function subscriptionReducer(state: SubscriptionState | undefined, action: Action) {
    return reducer(state, action);
}
