import { SUBSCRIPTION_PLANS } from '../constants/subscription.constants';
import { CloudSubscriptionPlan } from '../types/cloud-subscription-plan.interface';
import { CloudSubscription } from '../types/cloud-subscription.interface';

export const getApplicableSubscriptionPlan = (subscription: CloudSubscription): CloudSubscriptionPlan =>
    SUBSCRIPTION_PLANS.find((p) => p.durationMonths === subscription.duration);

export const computeSubscriptionFinalAmount = (subscription: CloudSubscription): number => {
    const plan = getApplicableSubscriptionPlan(subscription);
    const amount = subscription.gigabyte * plan.priceUsdPerGb;
    return subscription.upfrontPayment ? amount * 0.9 : amount;
};
