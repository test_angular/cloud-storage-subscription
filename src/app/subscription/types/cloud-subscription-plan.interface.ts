export interface CloudSubscriptionPlan {
    durationMonths: number;
    priceUsdPerGb: number;
}
