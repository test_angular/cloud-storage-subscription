export interface Step {
    step: number;
    title: string;
    path: string;
}
