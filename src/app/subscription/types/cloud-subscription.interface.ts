import { CloudSubscriptionPlan } from './cloud-subscription-plan.interface';

export interface Payment {
    amount: number;
    creditCardNo: string;
    creditCardExpiration: string;
    creditCardSecurityCode: string;
}

export interface CloudSubscription {
    duration: 3 | 6 | 12;
    gigabyte: 5 | 10 | 50;
    upfrontPayment: boolean;
    payment: Payment;
    plan: CloudSubscriptionPlan;
    finalAmount: number;
    clientEmail: string;
}
