import { CloudSubscriptionPlan } from '../types/cloud-subscription-plan.interface';
import { CloudSubscription } from '../types/cloud-subscription.interface';

export const DEFAULT_SUBSCRIPTION: CloudSubscription = {
    duration: 12,
    gigabyte: 5,
    upfrontPayment: false,
    payment: null,
    clientEmail: null,
    finalAmount: null,
    plan: null,
};

export const SUBSCRIPTION_DURATION_OPTIONS: number[] = [3, 6, 12];

export const SUBSCRIPTION_GIGABYTE_OPTIONS: number[] = [5, 10, 50];

export const SUBSCRIPTION_PLANS: CloudSubscriptionPlan[] = [
    { durationMonths: 3, priceUsdPerGb: 3 },
    { durationMonths: 6, priceUsdPerGb: 2.5 },
    { durationMonths: 12, priceUsdPerGb: 2 },
];
