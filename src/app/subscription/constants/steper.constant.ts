import { Step } from '../types/step.interface';

export const STEPS: Step[] = [
    { path: '/subscription/options', step: 1, title: 'Subscription options' },
    { path: '/subscription/payment', step: 2, title: 'Payment' },
    { path: '/subscription/confirmation', step: 3, title: 'Confirmation' },
];
