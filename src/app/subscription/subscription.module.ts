import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';
import { StepperComponent } from './components/stepper/stepper.component';
import { SubscriptionConfirmationComponent } from './components/subscription-confirmation/subscription-confirmation.component';
import { SubscriptionOptionsComponent } from './components/subscription-options/subscription-options.component';
import { SubscriptionPaymentComponent } from './components/subscription-payment/subscription-payment.component';
import { SuccessfulSubmitComponent } from './components/successful-submit/successful-submit.component';
import { SummaryComponent } from './components/summary/summary.component';
import { ValidationErrorComponent } from './components/validation-error/validation-error.component';
import { SubscriptionConfirmationRootComponent } from './containers/subscription-confirmation-root/subscription-confirmation-root.component';
import { SubscriptionOptionsRootComponent } from './containers/subscription-options-root/subscription-options-root.component';
import { SubscriptionPaymentRootComponent } from './containers/subscription-payment-root/subscription-payment-root.component';
import { SubscriptionRootComponent } from './containers/subscription-root/subscription-root.component';
import { PaymentInfosReceivedGuard } from './guards/payment-infos-received.guard';
import { SubscriptionMockService } from './services/subscription-mock.service';
import { SubscriptionEffects } from './store/effects/subscription.effects';
import { subscriptionReducer } from './store/reducers/subscription.reducers';
import { SubscriptionRoutingModule } from './subscription-routing.module';

@NgModule({
    declarations: [
        SubscriptionOptionsRootComponent,
        SubscriptionPaymentRootComponent,
        SubscriptionConfirmationRootComponent,
        SubscriptionOptionsComponent,
        SubscriptionPaymentComponent,
        SubscriptionConfirmationComponent,
        SubscriptionRootComponent,
        StepperComponent,
        ValidationErrorComponent,
        SummaryComponent,
        SuccessfulSubmitComponent,
    ],
    imports: [
        CommonModule,
        SubscriptionRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forFeature('subscription', subscriptionReducer),
        EffectsModule.forFeature([SubscriptionEffects]),
        NgxMaskDirective,
        NgxMaskPipe,
    ],
    providers: [provideNgxMask(), SubscriptionMockService, PaymentInfosReceivedGuard],
})
export class SubscriptionModule {}
