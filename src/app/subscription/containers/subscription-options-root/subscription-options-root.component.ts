import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { SubSink } from 'subsink';
import { saveSubscriptionToStore } from '../../store/actions/subscription.action';
import { SubscriptionState } from '../../store/reducers/subscription.reducers';
import { getSubscription } from '../../store/selectors/subscription.selectors';
import { CloudSubscription } from '../../types/cloud-subscription.interface';

@Component({
    selector: 'app-subscription-options-root',
    templateUrl: './subscription-options-root.component.html',
    styleUrls: ['./subscription-options-root.component.scss'],
})
export class SubscriptionOptionsRootComponent implements OnInit {
    subscription: CloudSubscription;
    private subs = new SubSink();

    constructor(private subscriptionStore: Store<SubscriptionState>) {}

    ngOnInit(): void {
        this.subs.sink = this.subscriptionStore
            .pipe(select(getSubscription))
            .subscribe((subscription) => (this.subscription = subscription));
    }

    onFormChange(subscription: CloudSubscription) {
        this.subscriptionStore.dispatch(
            saveSubscriptionToStore({
                payload: {
                    ...this.subscription,
                    ...subscription,
                },
            })
        );
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }
}
