import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { SubSink } from 'subsink';
import { createSubscription } from '../../store/actions/subscription.action';
import { SubscriptionState } from '../../store/reducers/subscription.reducers';
import { getSubscription } from '../../store/selectors/subscription.selectors';
import { CloudSubscription } from '../../types/cloud-subscription.interface';

@Component({
    selector: 'app-subscription-confirmation-root',
    templateUrl: './subscription-confirmation-root.component.html',
    styleUrls: ['./subscription-confirmation-root.component.scss'],
})
export class SubscriptionConfirmationRootComponent {
    subscription: CloudSubscription;
    private subs = new SubSink();

    constructor(private subscriptionStore: Store<SubscriptionState>) {}

    ngOnInit(): void {
        this.subs.sink = this.subscriptionStore
            .pipe(select(getSubscription))
            .subscribe((subscription) => (this.subscription = subscription));
    }

    onConfirm(email: string) {
        this.subscriptionStore.dispatch(
            createSubscription({
                payload: {
                    ...this.subscription,
                    clientEmail: email,
                },
            })
        );
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }
}
