import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { SubSink } from 'subsink';
import { saveSubscriptionToStore } from '../../store/actions/subscription.action';
import { SubscriptionState } from '../../store/reducers/subscription.reducers';
import { getSubscription } from '../../store/selectors/subscription.selectors';
import { CloudSubscription, Payment } from '../../types/cloud-subscription.interface';

@Component({
    selector: 'app-subscription-payment-root',
    templateUrl: './subscription-payment-root.component.html',
    styleUrls: ['./subscription-payment-root.component.scss'],
})
export class SubscriptionPaymentRootComponent {
    subscription: CloudSubscription;
    private subs = new SubSink();

    constructor(private subscriptionStore: Store<SubscriptionState>, private router: Router) {}

    ngOnInit(): void {
        this.subs.sink = this.subscriptionStore
            .pipe(select(getSubscription))
            .subscribe((subscription) => (this.subscription = subscription));
    }

    onSubmit(payment: Payment) {
        this.subscriptionStore.dispatch(
            saveSubscriptionToStore({
                payload: {
                    ...this.subscription,
                    payment,
                },
            })
        );
        this.router.navigate(['/subscription/confirmation']);
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }
}
