import { Component } from '@angular/core';
import { STEPS } from '../../constants/steper.constant';

@Component({
    selector: 'app-subscription-root',
    templateUrl: './subscription-root.component.html',
    styleUrls: ['./subscription-root.component.scss'],
})
export class SubscriptionRootComponent {
    steps = STEPS;
}
