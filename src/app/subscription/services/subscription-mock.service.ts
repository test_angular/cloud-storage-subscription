import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CloudSubscription } from '../types/cloud-subscription.interface';

@Injectable({
    providedIn: 'root',
})
export class SubscriptionMockService {
    createSubscription(subscription: CloudSubscription): Observable<CloudSubscription> {
        return of(subscription);
    }
}
