import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'subscription', pathMatch: 'full' },
    {
        path: 'subscription',
        loadChildren: () => import('./subscription/subscription.module').then((m) => m.SubscriptionModule),
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
